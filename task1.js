function adjacentElementsProduct(inputArray) {
    let lp = Number.NEGATIVE_INFINITY;
    for(let i=0;i<inputArray.length-1;i++){
        if(inputArray[i]*inputArray[i+1] > cb){
            lp = inputArray[i]*inputArray[i+1];
        }
    }
    return lp;
}

console.log(adjacentElementsProduct([-1, -2]));
console.log(adjacentElementsProduct([5, 1, 2, 3, 1, 4]));
console.log(adjacentElementsProduct([1, 2, 3, 0]));
console.log(adjacentElementsProduct([9, 5, 10, 2, 24, -1, -48]));