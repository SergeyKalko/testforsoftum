data = ["X.O",
    "XX.",
    "XOO"];
function xoReferee(data) {
    let winner = "";
    data[0] = data[0].split('');
    data[1] = data[1].split('');
    data[2] = data[2].split('');

    for (let i = 0; i < 3; i++) {
        //checking horizontally
        if (data[i][0] == data[i][1] && data[i][1] == data[i][2]) {
            winner = data[i][0];
        }
        //checking vertically
        else if (data[0][i] == data[1][i] && data[1][i] == data[2][i]) {
            winner = data[0][i];
        }
        //checking diagnolly
        else if (data[0][0] == data[1][1] && data[1][1] == data[2][2]) {
            winner = data[0][0];
        } else if (data[0][2] == data[1][1] && data[1][1] == data[2][0]) {
            winner = data[0][2];
        }
    }

    if (winner != "X" && winner != "O") {
        winner = "D";
    }

    return winner;
}
console.log(xoReferee(data));